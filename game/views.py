from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from django.views import generic
from .models import Warrior

# Create your views here.

class WarProfile(generic.DetailView):
    model = Warrior

def army(request, userid):
    user = get_object_or_404(User, pk=userid)
    warriors = Warrior.objects.filter(user=user)
    context = {
            'user': user,
            'warriors': warriors
            }
    return render(request, 'game/army.html', context)
